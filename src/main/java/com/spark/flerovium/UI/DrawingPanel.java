package com.spark.flerovium.UI;

import javax.swing.JPanel;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseEvent;
import java.awt.Point;
import java.awt.Graphics;
import java.util.ArrayList;

public class DrawingPanel extends JPanel implements MouseListener, MouseMotionListener {
    private ArrayList<Point> points;

    public DrawingPanel() {
	points = new ArrayList<Point>();
	addMouseListener(this);
	addMouseMotionListener(this);
    }

    @Override
    protected void paintComponent(Graphics g) {
	super.paintComponent(g);

	for (int i = 0; i < points.size() - 2; i++) {
	    Point p1 = points.get(i);
	    Point p2 = points.get(i + 1);

	    g.drawLine(p1.x, p1.y, p2.x, p2.y);
	}
    }

    @Override
    public void mouseDragged(MouseEvent e) {
	points.add(e.getPoint());
	repaint(); // todo: causes unnecessary paint processing ; use other repaint()
    }

    @Override
    public void mouseMoved(MouseEvent e) {}

    @Override
    public void mouseClicked(MouseEvent e) {}

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void	mouseExited(MouseEvent e) {}

    @Override
    public void	mousePressed(MouseEvent e) {
	points.add(e.getPoint());
    }

    @Override
    public void	mouseReleased(MouseEvent e) {
	points.add(e.getPoint());
    }
}
