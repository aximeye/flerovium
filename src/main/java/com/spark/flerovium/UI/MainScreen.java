package com.spark.flerovium.UI;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JToolBar;

public class MainScreen extends JFrame {
    private DrawingPanel drawingPanel;
    private JButton pencilButton;
    private JButton brushButton;
    private JMenu fileMenuItem;
    private JMenu editMenuItem;
    private JMenuBar menuBar;
    private JToolBar toolBar;

    private void initComponents() {
        drawingPanel = new DrawingPanel();
	toolBar = new javax.swing.JToolBar();
        pencilButton = new javax.swing.JButton();
        brushButton = new javax.swing.JButton();
        menuBar = new javax.swing.JMenuBar();
        fileMenuItem = new javax.swing.JMenu();
        editMenuItem = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        drawingPanel.setBackground(new java.awt.Color(255, 255, 255));
        drawingPanel.setToolTipText("");

        javax.swing.GroupLayout drawingPanelLayout = new javax.swing.GroupLayout(drawingPanel);
        drawingPanel.setLayout(drawingPanelLayout);
        drawingPanelLayout.setHorizontalGroup(
					      drawingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
					      .addGap(0, 0, Short.MAX_VALUE)
					      );
        drawingPanelLayout.setVerticalGroup(
					    drawingPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
					    .addGap(0, 440, Short.MAX_VALUE)
					    );

        toolBar.setFloatable(false);
        toolBar.setRollover(true);

        pencilButton.setText("Pencil");
        pencilButton.setToolTipText("Pencil");
        pencilButton.setFocusable(false);
        pencilButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        pencilButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(pencilButton);

        brushButton.setText("Brush");
        brushButton.setToolTipText("Brush");
        brushButton.setFocusable(false);
        brushButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        brushButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(brushButton);

        fileMenuItem.setText("File");
        menuBar.add(fileMenuItem);

        editMenuItem.setText("Edit");
        menuBar.add(editMenuItem);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
				  layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				  .addGroup(layout.createSequentialGroup()
					    .addComponent(toolBar, javax.swing.GroupLayout.DEFAULT_SIZE, 672, Short.MAX_VALUE)
					    .addContainerGap())
				  .addComponent(drawingPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
				  );
        layout.setVerticalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					  .addComponent(toolBar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
					  .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
					  .addComponent(drawingPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				);

        pack();

    }

    public MainScreen() {
	initComponents();
    }
}
